//
//  AppDelegate.h
//  OSX
//
//  Created by Szymon Żyrek on 05/04/15.
//  Copyright (c) 2015 Szymon Żyrek. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

