IF YOU READ THIS
==========
 
No point in creating a full-blown README right now, too early and too much stuff will change soon. When some "core" of the project will stabilize ill put more info here (config params, api's,etc). For now- if you want to know anything, just contact me (szymon.zyrek@gmail.com), and ill provide you with info.

Anyway, this is a very early version of best computer game engine in the world, in short. For now it's not much more than a simple renderer, couple of very simple shaders and some "other stuff" to bind it together. 

Config params (with example values):

DEBUG_KEYS=OBJECT_CREATION,EVENTS,OBJECT_DESTRUCTION,LOGGING,COPY_CONSTRUCTORS,RENDERING,ACCUMULATOR,GL_ERRORS,SHADERS,TEXTURES
DEBUG_TO_CONSOLE=YES
DEBUG_TO_FILE=YES
SHOW_DEBUG_KEY=NO
ADDITIONAL_CONFIG_FILES=C:\\temp\\additional.conf
LOG_FILE=C:\\temp\\game10.log
_CRT_SECURE_NO_WARNINGS
DEFAULT_MODEL=globe.obj
DEFAULT_TEXTURE=grass.jpg

Sry for messy formatting and not much info, no time now. Again, just contact me.