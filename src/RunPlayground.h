//
//  RunPlayground.h
//  Game10
//
//  Created by Szymon Żyrek on 30/10/14.
//  Copyright (c) 2014 Szymon Żyrek. All rights reserved.
//

#ifndef __Game10__RunPlayground__
#define __Game10__RunPlayground__

class RunPlayground {
public:
    static void p_main();
};

#endif /* defined(__Game10__RunPlayground__) */
