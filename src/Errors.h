#ifndef __ERRORS__GUARD__
#define __ERRORS__GUARD__

enum ErrorCodes{
	GAMEOBJECTS_OVERFLOW,
	AI_OVERFLOW,
	RENDERABLES_OVERFLOW,
	BODIES_OVERFLOW,
	INPUTS_OVERFLOW,
	REGISTERING_REGISTERED_OBJECT
};
#endif